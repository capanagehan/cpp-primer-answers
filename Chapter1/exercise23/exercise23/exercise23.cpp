// exercise 1.23: Write a program that reads several transactions and counts how many transactions occur for each ISBN.

#include <iostream>
#include "Sales_item.h"

int main()
{
    Sales_item curItem, var;
    if (std::cin >> curItem)
    {
        int sum = 1;
        while (std::cin >> var)
        {
            if (curItem.isbn() == var.isbn())
            {
                ++sum;
            }
           else
            {
                std::cout << "number of copies " << curItem.isbn() << " are " << sum << std::endl;
                curItem = var;
                sum = 1;
            }
        }
        std::cout << "number of copies " << curItem.isbn() << " are " << sum << std::endl;
    }
    else
    {
        std::cerr << "No data!" << std::endl;
        return -1;
    }
    return 0;
} 