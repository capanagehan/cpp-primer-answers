// exercise 1.13: Rewrite the exercises 1.9 and 1.10 using for loops.

#include <iostream>

int main()
{
    // exercise 1.9
    int sum = 0;
    for (int val = 50; val <= 100; ++val)
        sum += val;
    std::cout << "sum of 50 to 100 inclusive is " << sum << std::endl;
    
    // exercise 1.10
    for (int val = 10; val >= 0; --val)
        std::cout << val << std::endl;

    return 0;
}