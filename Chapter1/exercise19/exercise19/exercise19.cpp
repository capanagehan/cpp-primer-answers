// exercise 1.19: Revise the program you wrote for the exercise 1.11  that 
// printed a range of numbers so that it handles input in which the first number is smaller than the second.

// exercise 1.11: Write a program that prompts the user for two integers. Print each number in the range specified by those two integers.  

#include <iostream>

int main()
{
    std::cout << "Enter two numbers: " << std::endl;
    int low = 0, high = 0;
    std::cin >> low >> high;
    if (low <= high)
    {
        while (low <= high)
        {
            std::cout << low << std::endl;
            ++low;
        }
    }
    else
    {
        while (high <= low)
        {
            std::cout << high << std::endl;
            ++high;
        }
    }

    return 0;
}
