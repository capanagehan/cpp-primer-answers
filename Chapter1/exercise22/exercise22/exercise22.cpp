// exercise 1.22: Write a program that reads several transactions for the same ISBN. 
// Write the sum of all the transactions that were read.

#include <iostream>
#include "Sales_item.h"

int main()
{
    Sales_item bookSum, book;
    while (std::cin >> book)
    {
        bookSum += book;
    }
    std::cout << bookSum << std::endl;

    return 0;
}
