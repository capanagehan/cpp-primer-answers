// exercise 1.11: Write a program that prompts the user for two integers. Print each number in the range specified by those two integers.  

#include <iostream>

int main()
{
    std::cout << "Enter two numbers: " << std::endl;
    int low = 0, high = 0;
    std::cin >> low >> high;
    int mt = low;
    while (mt <= high)
    {
        std::cout << mt << std::endl;
        ++mt;
    }
    while (low >= high)
    {
        std::cout << high << std::endl;
        ++high;
    }

    return 0;
}
