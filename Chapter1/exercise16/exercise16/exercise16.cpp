// exercise 1.16: Write your own version of a program that prints the sum of a set of integers read from cin.

#include <iostream>

int main()
{
    std::cout << "Enter some numbers to sum: " << std::endl;
    std::cout << "Press a key that is not a number to quit." << std::endl;
    int sum = 0, value = 0;
    while (std::cin >> value)
    {
        sum += value;
    }
    std::cout << "The sum of numbers is: " << sum << std::endl;

    return 0;
}