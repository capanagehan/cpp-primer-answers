// exercise 1.5: Rewrite the program on the page 6 to use a seperate statement to print each operand.

#include <iostream>

int main()
{
    std::cout << "Enter two numbers: " << std::endl;
    int v1 = 0, v2 = 0;
    std::cin >> v1 >> v2;
    std::cout << "The sum of " << v1;
    std::cout << " and " << v2;
    std::cout << " is " << v1 + v2;
    std::cout << std::endl;

    return 0;
}

